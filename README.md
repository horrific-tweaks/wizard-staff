**[Watch the introductory/tutorial video for this datapack here!](https://youtu.be/JoTcxrZ2nu4)**

This datapack adds a mysterious wizard staff to the game - which can rotate blocks, change grass types, and other random unexpected things.

The staff is based on a custom `carrot_on_a_stick` item, which can be obtained by placing an end crystal on top of an obsidian pyramid. The pyramid must have a 5x5 layer of obsidian, followed by a 3x3 layer of crying obsidian, and one block of obsidian on top.

The wizard staff will not be compatible with any other datapacks that make use of the `debug_stick` item.

<details>
  <summary>Spoilers</summary>

  This datapack also makes use of the [Illusioner](https://minecraft.fandom.com/wiki/Illusioner) mob, which spawns when an end crystal is placed on the pyramid.
</details>

## Credits

This pack was initially based on [vanilla-friendly-datapacks/rotation-wrench](https://github.com/vanilla-friendly-datapacks/rotation-wrench/). As such, it retains the GPLv3 license.
