# Used for sculk xp checks
forceload add 0 0
scoreboard objectives add wiz_staff.xp xp
# Used for raycasting.
scoreboard objectives add wiz_staff.cast dummy
scoreboard objectives add wiz_staff.end_c minecraft.used:minecraft.end_crystal
scoreboard objectives add wiz_staff.used minecraft.used:minecraft.<< items.staff.id >>
