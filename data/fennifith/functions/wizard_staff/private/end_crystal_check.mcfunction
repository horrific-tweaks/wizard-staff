<% set conditions %>
if block ~ ~-1 ~ obsidian
if block ~ ~-1 ~1 air
if block ~ ~-1 ~-1 air
if block ~1 ~-1 ~ air
if block ~1 ~-1 ~1 air
if block ~1 ~-1 ~-1 air
if block ~-1 ~-1 ~ air
if block ~-1 ~-1 ~1 air
if block ~-1 ~-1 ~-1 air
if block ~ ~-2 ~ crying_obsidian
if block ~ ~-2 ~1 crying_obsidian
if block ~ ~-2 ~-1 crying_obsidian
if block ~1 ~-2 ~ crying_obsidian
if block ~1 ~-2 ~1 crying_obsidian
if block ~1 ~-2 ~-1 crying_obsidian
if block ~-1 ~-2 ~ crying_obsidian
if block ~-1 ~-2 ~1 crying_obsidian
if block ~-1 ~-2 ~-1 crying_obsidian
<% endset %>
<% set conditions = conditions | trim %>
<% set conditions = conditions.split('\n') %>

scoreboard players set #valid wiz_staff.cast 0
<% for condition in conditions | batch(4) %>
execute if score #valid wiz_staff.cast matches << loop.index - 1 >> << condition | join(' ') >> run scoreboard players set #valid wiz_staff.cast << loop.index >>
<% endfor %>

<% set expected = conditions|length // 4 + 1 %>
execute unless score #valid wiz_staff.cast matches << expected >> run tag @s add wiz_checked
execute if score #valid wiz_staff.cast matches << expected >> if score #schedule wiz_staff.cast matches 1 run function fennifith:wizard_staff/private/end_crystal_schedule
execute if score #valid wiz_staff.cast matches << expected >> if score #schedule wiz_staff.cast matches 0 run function fennifith:wizard_staff/private/end_crystal_summon
