# if the player activates the search (e.g. by placing the crystal), run the schedule function
execute if entity @s[type=minecraft:player] run scoreboard players set #schedule wiz_staff.cast 1
# otherwise, this function was already scheduled, so it should run end_crystal_summon
execute unless entity @s[type=minecraft:player] run scoreboard players set #schedule wiz_staff.cast 0

execute as @e[type=minecraft:end_crystal,tag=!wiz_checked] at @s run function fennifith:wizard_staff/private/end_crystal_check
