kill @s
summon minecraft:illusioner ~ ~ ~

playsound entity.lightning_bolt.impact weather @a ~ ~ ~
playsound entity.wither.spawn weather @a ~ ~ ~

particle minecraft:explosion_emitter ~ ~1 ~

effect give @a[distance=..20] minecraft:blindness 30
