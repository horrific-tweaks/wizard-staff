# Executes in a loop until a given block has been found.
execute unless block ~ ~ ~ #fennifith:wizard_staff/ignore run function fennifith:wizard_staff/private/raycast/hit_block
scoreboard players add #distance wiz_staff.cast 1
execute if score #hit wiz_staff.cast matches 0 if score #distance wiz_staff.cast matches ..45 positioned ^ ^ ^0.1 run function fennifith:wizard_staff/private/raycast/ray
