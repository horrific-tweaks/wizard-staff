# Players that raycast can be found with this tag.
tag @s add wizard_staff.raycasting

# Initialize and start raycasting.
scoreboard players set #hit wiz_staff.cast 0
scoreboard players set #distance wiz_staff.cast 0
function fennifith:wizard_staff/private/raycast/ray

# When done, remove the raycasting tag.
tag @s remove wizard_staff.raycasting
