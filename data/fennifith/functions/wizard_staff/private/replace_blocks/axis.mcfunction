<% set blockset = blocksets.axis %>
# << blockset.id >>

# A flag is needed to prevent the turn logic to do a full cycle on each trigger,
# resulting the block in being in the same orientation as before.
execute as @s[tag=!wizard_staff.preventRecursion] if block ~ ~ ~ << blockset.id >>[<< blockset.states|last >>] run tag @s add wizard_staff.preventRecursion
<% for block in blockset.blocks %>
#   | block: << block >>
<% for i in range(0, blockset.states|length) | reverse %>
<% set state = blockset.states[i] %>
<% if loop.first %><% set nextstate = blockset.states[0] %>
<% else %><% set nextstate = blockset.states[i+1] %>
<% endif %>
<% if loop.first %>
execute as @s[tag=wizard_staff.preventRecursion] if block ~ ~ ~ minecraft:<< block >>[<< state >>] run setblock ~ ~ ~ minecraft:<< block >>[<< nextstate >>]
<% else %>
execute as @s[tag=!wizard_staff.preventRecursion] if block ~ ~ ~ minecraft:<< block >>[<< state >>] run setblock ~ ~ ~ minecraft:<< block >>[<< nextstate >>]
<% endif %>
<% endfor %>
<% endfor %>

# Play rotation sound.
playsound block.chorus_flower.grow block @a ~ ~ ~
