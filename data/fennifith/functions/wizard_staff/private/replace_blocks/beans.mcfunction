# summon a bean can (see: james/minecraft-bean-cans)
execute if block ~ ~ ~ minecraft:cocoa[age=2] run summon item ~ ~ ~ {Item:{id:"minecraft:rotten_flesh",Count:1b,tag:{display:{Name:'{"text":"Inconspicuous Bean Can","italic":false}'},CustomModelData:817252},PickupDelay:20}}

execute if block ~ ~ ~ minecraft:cocoa[age=2] run particle minecraft:poof ~ ~.5 ~
execute if block ~ ~ ~ minecraft:cocoa[age=2] run particle minecraft:poof ~.3 ~.5 ~.3
execute if block ~ ~ ~ minecraft:cocoa[age=2] run particle minecraft:poof ~-.3 ~.5 ~-.3

execute if block ~ ~ ~ minecraft:cocoa[age=2] run playsound item.armor.equip_turtle block @a ~ ~ ~
execute if block ~ ~ ~ minecraft:cocoa[age=2] run setblock ~ ~ ~ minecraft:air

# if less than age 2, run unsupported behavior
execute if block ~ ~ ~ minecraft:cocoa[age=1] run function fennifith:wizard_staff/private/replace_blocks/unsupported
execute if block ~ ~ ~ minecraft:cocoa[age=0] run function fennifith:wizard_staff/private/replace_blocks/unsupported
