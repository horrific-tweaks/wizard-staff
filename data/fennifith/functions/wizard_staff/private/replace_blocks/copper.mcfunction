execute if block ~ ~ ~ weathered_copper run setblock ~ ~ ~ oxidized_copper
execute if block ~ ~ ~ waxed_weathered_copper run setblock ~ ~ ~ oxidized_copper
execute if block ~ ~ ~ exposed_copper run setblock ~ ~ ~ weathered_copper
execute if block ~ ~ ~ waxed_exposed_copper run setblock ~ ~ ~ weathered_copper
execute if block ~ ~ ~ copper_block run setblock ~ ~ ~ exposed_copper
execute if block ~ ~ ~ waxed_copper_block run setblock ~ ~ ~ exposed_copper

execute align xyz run particle minecraft:wax_off ~.5 ~.5 ~.5 .5 .5 .5 1 10
playsound minecraft:item.axe.scrape block @a
