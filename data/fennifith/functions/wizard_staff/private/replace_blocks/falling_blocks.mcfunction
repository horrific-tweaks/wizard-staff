<% set blockset = blocksets.falling_blocks %>

# Using a leash knot to find the center of the block...
summon minecraft:leash_knot ~ ~ ~ {Tags:["Center"]}
<% set condition = "if block ~ ~1 ~ minecraft:air if block ~ ~2 ~ minecraft:air" %>

# Make the sand fly upwards (should fall back in the same spot)
<% for block in blockset.blocks %>
execute at @e[tag=Center,limit=1] if block ~ ~ ~ minecraft:<< block >> << condition >> run summon minecraft:falling_block ~ ~ ~ {BlockState:{Name:"minecraft:<< block >>"},Motion:[0.0d,1.0d,0.0d],Time:1}
<% endfor %>
kill @e[tag=Center]

execute << condition >> run setblock ~ ~ ~ minecraft:air

execute << condition >> run playsound entity.evoker.cast_spell block @a ~ ~ ~
