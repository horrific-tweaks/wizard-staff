# A flag is needed to prevent the turn logic to do a full cycle on each trigger,
# resulting the block in being in the same orientation as before.
execute as @s[tag=!wizard_staff.preventRecursion] if block ~ ~ ~ minecraft:grass_block run tag @s add wizard_staff.preventRecursion

# can transform grass into mycelium
execute as @s[tag=wizard_staff.preventRecursion] if block ~ ~ ~ minecraft:grass_block run setblock ~ ~ ~ minecraft:mycelium
execute as @s[tag=!wizard_staff.preventRecursion] if block ~ ~ ~ minecraft:mycelium run setblock ~ ~ ~ minecraft:grass_block

# Play rotation sound.
playsound minecraft:item.shovel.flatten block @a ~ ~ ~
