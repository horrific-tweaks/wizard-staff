# create a constant sculk_catalyst at 0,0
execute unless block 0 -64 0 minecraft:sculk_catalyst run setblock 0 -64 0 minecraft:sculk_catalyst

# create a temporary charge data entry
data modify storage wizard_staff:sculk charge set value {decay_delay:100,update_delay:10,facings:["up"],charge:10}
# set the charge position from current location marker entity
kill @e[type=marker,tag=wiz.sculk]
summon minecraft:marker ~ ~ ~ {Tags:["wiz.sculk"]}

data modify storage wizard_staff:sculk charge.pos set value [0,0,0]
execute store result storage wizard_staff:sculk charge.pos[0] int 1 run data get entity @e[type=marker,tag=wiz.sculk,sort=nearest,limit=1] Pos[0]
execute store result storage wizard_staff:sculk charge.pos[1] int 1 run data get entity @e[type=marker,tag=wiz.sculk,sort=nearest,limit=1] Pos[1]
execute store result storage wizard_staff:sculk charge.pos[2] int 1 run data get entity @e[type=marker,tag=wiz.sculk,sort=nearest,limit=1] Pos[2]

kill @e[type=marker,tag=wiz.sculk]

# add charge to sculk block
execute if entity @s[scores={wiz_staff.xp=10..}] run data modify block 0 -64 0 cursors append from storage wizard_staff:sculk charge
# remove 10xp from the player
execute if entity @s[scores={wiz_staff.xp=10..}] run xp add @s -10 points
