# cactus shoots spikes in all directions
setblock ~ ~ ~ minecraft:air

<% set coords = "0.4,0.2 0.2,0.4".split(" ") %>
<% for set in coords %>
<% set x, z = set.split(",") %>
<% set y = (x|float + z|float) / 2 %>
summon arrow ~0 ~0 ~0 {Motion:[<< x >>,<< y >>,<< z >>],CustomName:'"an exploding cactus"'}
summon arrow ~0 ~0 ~0 {Motion:[-<< x >>,<< y >>, << z >>],CustomName:'"an exploding cactus"'}
summon arrow ~0 ~0 ~0 {Motion:[<< x >>,<< y >>, -<< z >>],CustomName:'"an exploding cactus"'}
summon arrow ~0 ~0 ~0 {Motion:[-<< x >>,<< y >>, -<< z >>],CustomName:'"an exploding cactus"'}
summon arrow ~0 ~0 ~0 {Motion:[<< z >>,<< y >>,<< x >>],CustomName:'"an exploding cactus"'}
summon arrow ~0 ~0 ~0 {Motion:[-<< z >>,<< y >>,<< x >>],CustomName:'"an exploding cactus"'}
summon arrow ~0 ~0 ~0 {Motion:[<< z >>,<< y >>,-<< x >>],CustomName:'"an exploding cactus"'}
summon arrow ~0 ~0 ~0 {Motion:[-<< z >>,<< y >>,-<< x >>],CustomName:'"an exploding cactus"'}
<% endfor %>

particle minecraft:poof ~ ~.5 ~
particle minecraft:poof ~.3 ~.5 ~.3
particle minecraft:poof ~-.3 ~.5 ~-.3
playsound minecraft:item.crossbow.shoot block @a ~ ~ ~
