# can dry used sponges
execute if block ~ ~ ~ minecraft:wet_sponge run playsound block.fire.extinguish block @a ~ ~ ~

execute if block ~ ~ ~ minecraft:wet_sponge run particle minecraft:poof ~ ~.5 ~
execute if block ~ ~ ~ minecraft:wet_sponge run particle minecraft:poof ~.3 ~.5 ~.3
execute if block ~ ~ ~ minecraft:wet_sponge run particle minecraft:poof ~-.3 ~.5 ~-.3

execute if block ~ ~ ~ minecraft:wet_sponge run setblock ~ ~ ~ minecraft:sponge
