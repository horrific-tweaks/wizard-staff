execute unless block ~ ~ ~ #fennifith:wizard_staff/rotatable run function fennifith:wizard_staff/private/replace_blocks/unsupported

<% for key, set in blocksets %>
execute if block ~ ~ ~ << set.id >><< set.data >><% if set.not_data %> unless data block ~ ~ ~ << set.not_data >><%endif%> run function fennifith:wizard_staff/private/replace_blocks/<< key >>
<% endfor %>

# Remove recursion tag.
execute as @s run tag @s remove wizard_staff.preventRecursion
