# Why'd you use it on water!? Now the wizard staff is soggy...
execute if entity @s[nbt={SelectedItem:{id:"minecraft:<< items.staff.id >>",tag:<< items.staff.nbt >>,Count:1b}}] run item replace entity @s weapon.mainhand with minecraft:<< items.soggy_staff.id >><< items.soggy_staff.nbt_full >>
playsound minecraft:item.bucket.fill block @a ~ ~ ~
particle minecraft:bubble_pop ~ ~.5 ~
particle minecraft:bubble_pop ~.3 ~.5 ~.3
particle minecraft:bubble_pop ~-.3 ~.5 ~-.3
