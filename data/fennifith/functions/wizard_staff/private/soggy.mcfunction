execute if block ~ ~ ~ minecraft:campfire run function fennifith:wizard_staff/private/soggy_dry
execute unless block ~ ~ ~ minecraft:campfire run playsound entity.fish.swim block @a ~ ~ ~
execute unless block ~ ~ ~ minecraft:campfire run particle minecraft:bubble_pop ~.1 ~.5 ~-.4
execute unless block ~ ~ ~ minecraft:campfire run particle minecraft:bubble_pop ~.4 ~.5 ~.3
execute unless block ~ ~ ~ minecraft:campfire run particle minecraft:bubble_pop ~-.3 ~.5 ~-.2
