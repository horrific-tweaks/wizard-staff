scoreboard players set #slot wiz_staff.cast 99

<% for i in range(0, 4) | reverse %>
<% set condition %><% if i > 0 %>if data block ~ ~ ~ Items[<< i-1 >>] <% endif %>unless data block ~ ~ ~ Items[<< i >>]<% endset %>
execute << condition >> run scoreboard players set #slot wiz_staff.cast << i >>
<% endfor %>

<% for i in range(0, 4) | reverse %>
<% set condition %>if score #slot wiz_staff.cast matches << i >><% endset %>
execute << condition >> run data modify block ~ ~ ~ Items append value {Slot:<< i >>b,id:"minecraft:<< items.staff.id >>",tag:<< items.staff.nbt_full >>,Count:1b}
execute << condition >> run data modify block ~ ~ ~ CookingTimes[<< i >>] set value 1
execute << condition >> run data modify block ~ ~ ~ CookingTotalTimes[<< i >>] set value 600
<% endfor %>

execute unless score #slot wiz_staff.cast matches 99 run item replace entity @s weapon.mainhand with minecraft:air
execute if score #slot wiz_staff.cast matches 99 run playsound entity.fish.swim block @a ~ ~ ~
