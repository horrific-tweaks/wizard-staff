execute as @a[gamemode=survival,nbt={SelectedItem:{id:"minecraft:<< items.staff.id >>",tag:<< items.staff.nbt >>}}] if score @s wiz_staff.used matches 1.. run function fennifith:wizard_staff/private/init_ray
execute as @a[gamemode=survival,nbt={SelectedItem:{id:"minecraft:<< items.soggy_staff.id >>",tag:<< items.soggy_staff.nbt >>}}] if score @s wiz_staff.used matches 1.. run function fennifith:wizard_staff/private/init_ray
scoreboard players set @a wiz_staff.used 0

execute as @a at @s if score @s wiz_staff.end_c matches 1.. run function fennifith:wizard_staff/private/end_crystal_search
scoreboard players set @a wiz_staff.end_c 0

execute as @a[gamemode=survival,nbt={SelectedItem:{id:"minecraft:debug_stick",Count:1b}}] if entity @s[nbt=!{SelectedItem:{id:"minecraft:<< items.staff.id >>",tag:<< items.staff.nbt >>}}] if entity @s[nbt=!{SelectedItem:{id:"minecraft:<< items.soggy_staff.id >>",tag:<< items.soggy_staff.nbt >>}}] run function fennifith:wizard_staff/private/give
